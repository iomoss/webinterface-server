var cgi = require('cgi');
var http = require('http');
var path = require('path');

var port = 8080;
 
http.createServer(function(request, response) 
{
    console.log("Request for: " + request.url);

    var script = path.resolve(__dirname, '../cgi-bin' + request.url);
    console.log("Path: " + script);

    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Request-Method', '*');
    response.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
    response.setHeader('Access-Control-Allow-Headers', '*');

    return cgi(script)(request, response);
}).listen(port, function()
{
    console.log("Server started on port: " + port);
});
