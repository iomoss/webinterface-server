#!/bin/bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd $DIR

echo "Content-Type: text/plain"
echo

FILES=$(find sd_mount -type f | xargs du -sh)
echo "$FILES"
